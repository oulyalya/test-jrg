/**
 * проверяет валидность эмейла в форме подписки и показывает сообщение об отправке формы
 */
(function () {
  'use strict';

  if (document.querySelector('#subscribe')) {
    const form = document.querySelector('#subscribe');
    const userEmail = form.querySelector('#subscribe-email');

    userEmail.addEventListener('focus', () => {
      try {
        userEmail.value = localStorage.getItem('email');
      } catch {
        userEmail.focus();
      }
    })

    const resetIputClass = function (input, cssClass) {
      if (input.classList.contains(cssClass)) {

        setTimeout(function () {
          input.classList.remove(cssClass);
        }, 2000);
      }
    };

    const validateEmail = function (email) {
      const re = /\S+@\S+\.\S+/;
      return re.test(email.value);
    };

    const resetErrorClass = function () {
      resetIputClass(userEmail, 'form__error');
    };

    const validateForm = function () {
      if (!userEmail.value || !validateEmail(userEmail)) {
        userEmail.classList.add('form__error');
        userEmail.focus();
        return false;
      } else {
        return true;
      }
    };

    const submitHandler = function (evt) {
      evt.preventDefault();

      const isValid = validateForm();

      if (!isValid) {
        resetErrorClass();
        return false;
      } else {
        window.modal.showConfirmationMessage('#form-subscribe');
        userEmail.value = '';
        return true;
      }
    };

    if (form) {
      const submitBtn = form.querySelector('button[type=submit]');
      submitBtn.addEventListener('click', submitHandler);
    }
  }
})();

/**
 * сообщение об отправке формы жалобы
 */
(function () {
  'use strict';

  if (document.querySelector('#fyou')) {
    const wtf = document.querySelector('#fyou');
    const wtfForm = wtf.querySelector('.modal__content-wrapper')
    const options = wtfForm.querySelectorAll('button');

    const optionClickHandler = function (evt) {
      evt.preventDefault();
      window.modal.showConfirmationMessage('#fyou');

    }

    options.forEach(option => {
      option.addEventListener('click', optionClickHandler);
    })
  }
})();

/**
 * показывает сообщение при отправке формы в модальном окне
 */
(function () {
  'use strict';

  const showConfirmationMessage = function (mdl) {
    const modal = document.querySelector(mdl);

    if (modal) {
      const content = modal.querySelector('.modal__content-wrapper');
      const confirmation = modal.querySelector('.modal__reply-wrapper');

      if (modal.classList.contains('modal--reply-no-header')) {
        modal.classList.add('modal--js-reply-shown');
      }

      content.classList.add('modal__content-wrapper--js-invisible');
      setTimeout(() => {
        content.classList.add('modal__content-wrapper--js-hidden');
        content.classList.remove('modal__content-wrapper--js-invisible');
        confirmation.classList.add('modal__reply-wrapper--js-shown')
      }, 300)

      setTimeout(() => {
        confirmation.classList.add('modal__reply-wrapper--js-visible');
      }, 300)
    }
  }

  window.modal = {
    showConfirmationMessage: showConfirmationMessage,
  }
})();

/**
 * открытие-закрытие оверлея с подпиской и жалобой
 */
(function () {
  'use strict';

  if (document.querySelector('#overlay1')) {
    const overlay = document.querySelector('#overlay1');
    const btnImDeeplyInsulted = document.querySelector('#refuse');

    const escPressHandler = function (evt) {
      btnImDeeplyInsulted.addEventListener('click', showOverlayHandler);
      if (evt.key === 'Escape') {
        window.utils.hideModal(overlay, 'overlay--js-shown', 'overlay--js-visible');
      }
    };

    const showOverlayHandler = function (evt) {
      evt.preventDefault();

      window.utils.showModal(overlay, 'overlay--js-shown', 'overlay--js-visible');

      const closeBtn = overlay.querySelector('#overlay1-close');
      closeBtn.addEventListener('click', hideOverlayHandler);
      btnImDeeplyInsulted.removeEventListener('click', showOverlayHandler);
      document.addEventListener('keydown', escPressHandler);
    };

    const hideOverlayHandler = function () {
      const closeBtn = overlay.querySelector('#overlay1-close');
      closeBtn.removeEventListener('click', hideOverlayHandler);
      btnImDeeplyInsulted.addEventListener('click', showOverlayHandler);
      document.removeEventListener('keydown', escPressHandler);

      window.utils.hideModal(overlay, 'overlay--js-shown', 'overlay--js-visible');
    };

    btnImDeeplyInsulted.addEventListener('click', showOverlayHandler);
  }
})();

(function () {
  'use strict';

  const body = document.querySelector('body');

  const showModal = function
    (modal, cssClassDisplay, cssClassVisible) {

    modal.classList.add(cssClassDisplay);

    if (!modal.classList.contains(cssClassVisible)) {
      setTimeout(() => {
        modal.classList.add(cssClassVisible);
      }, 200);
    }

    if (body.offsetHeight > window.innerHeight) {
      body.classList.add('js-no-scroll');
    }
  }

  const hideModal = function (modal, cssClassDisplay, cssClassVisible) {
    if (modal.classList.contains(cssClassVisible)) {
      modal.classList.remove(cssClassVisible);
    }

    setTimeout(() => {
      modal.classList.remove(cssClassDisplay);
    }, 300);

    if (body.offsetHeight > window.innerHeight) {
      body.classList.remove('js-no-scroll');
    }

  }

  window.utils = {
    showModal: showModal,
    hideModal: hideModal,
  }
})();
