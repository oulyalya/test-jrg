/**
 * сообщение об отправке формы жалобы
 */
(function () {
  'use strict';

  if (document.querySelector('#fyou')) {
    const wtf = document.querySelector('#fyou');
    const wtfForm = wtf.querySelector('.modal__content-wrapper')
    const options = wtfForm.querySelectorAll('button');

    const optionClickHandler = function (evt) {
      evt.preventDefault();
      window.modal.showConfirmationMessage('#fyou');

    }

    options.forEach(option => {
      option.addEventListener('click', optionClickHandler);
    })
  }
})();
