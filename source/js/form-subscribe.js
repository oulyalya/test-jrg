/**
 * проверяет валидность эмейла в форме подписки и показывает сообщение об отправке формы
 */
(function () {
  'use strict';

  if (document.querySelector('#subscribe')) {
    const form = document.querySelector('#subscribe');
    const userEmail = form.querySelector('#subscribe-email');

    userEmail.addEventListener('focus', () => {
      try {
        userEmail.value = localStorage.getItem('email');
      } catch {
        userEmail.focus();
      }
    })

    const resetIputClass = function (input, cssClass) {
      if (input.classList.contains(cssClass)) {

        setTimeout(function () {
          input.classList.remove(cssClass);
        }, 2000);
      }
    };

    const validateEmail = function (email) {
      const re = /\S+@\S+\.\S+/;
      return re.test(email.value);
    };

    const resetErrorClass = function () {
      resetIputClass(userEmail, 'form__error');
    };

    const validateForm = function () {
      if (!userEmail.value || !validateEmail(userEmail)) {
        userEmail.classList.add('form__error');
        userEmail.focus();
        return false;
      } else {
        return true;
      }
    };

    const submitHandler = function (evt) {
      evt.preventDefault();

      const isValid = validateForm();

      if (!isValid) {
        resetErrorClass();
        return false;
      } else {
        window.modal.showConfirmationMessage('#form-subscribe');
        userEmail.value = '';
        return true;
      }
    };

    if (form) {
      const submitBtn = form.querySelector('button[type=submit]');
      submitBtn.addEventListener('click', submitHandler);
    }
  }
})();
