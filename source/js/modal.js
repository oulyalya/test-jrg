/**
 * показывает сообщение при отправке формы в модальном окне
 */
(function () {
  'use strict';

  const showConfirmationMessage = function (mdl) {
    const modal = document.querySelector(mdl);

    if (modal) {
      const content = modal.querySelector('.modal__content-wrapper');
      const confirmation = modal.querySelector('.modal__reply-wrapper');

      if (modal.classList.contains('modal--reply-no-header')) {
        modal.classList.add('modal--js-reply-shown');
      }

      content.classList.add('modal__content-wrapper--js-invisible');
      setTimeout(() => {
        content.classList.add('modal__content-wrapper--js-hidden');
        content.classList.remove('modal__content-wrapper--js-invisible');
        confirmation.classList.add('modal__reply-wrapper--js-shown')
      }, 300)

      setTimeout(() => {
        confirmation.classList.add('modal__reply-wrapper--js-visible');
      }, 300)
    }
  }

  window.modal = {
    showConfirmationMessage: showConfirmationMessage,
  }
})();
