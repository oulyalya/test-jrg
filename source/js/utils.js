(function () {
  'use strict';

  const body = document.querySelector('body');

  const showModal = function
    (modal, cssClassDisplay, cssClassVisible) {

    modal.classList.add(cssClassDisplay);

    if (!modal.classList.contains(cssClassVisible)) {
      setTimeout(() => {
        modal.classList.add(cssClassVisible);
      }, 200);
    }

    if (body.offsetHeight > window.innerHeight) {
      body.classList.add('js-no-scroll');
    }
  }

  const hideModal = function (modal, cssClassDisplay, cssClassVisible) {
    if (modal.classList.contains(cssClassVisible)) {
      modal.classList.remove(cssClassVisible);
    }

    setTimeout(() => {
      modal.classList.remove(cssClassDisplay);
    }, 300);

    if (body.offsetHeight > window.innerHeight) {
      body.classList.remove('js-no-scroll');
    }

  }

  window.utils = {
    showModal: showModal,
    hideModal: hideModal,
  }
})();
