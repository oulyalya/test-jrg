/**
 * открытие-закрытие оверлея с подпиской и жалобой
 */
(function () {
  'use strict';

  if (document.querySelector('#overlay1')) {
    const overlay = document.querySelector('#overlay1');
    const btnImDeeplyInsulted = document.querySelector('#refuse');

    const escPressHandler = function (evt) {
      btnImDeeplyInsulted.addEventListener('click', showOverlayHandler);
      if (evt.key === 'Escape') {
        window.utils.hideModal(overlay, 'overlay--js-shown', 'overlay--js-visible');
      }
    };

    const showOverlayHandler = function (evt) {
      evt.preventDefault();

      window.utils.showModal(overlay, 'overlay--js-shown', 'overlay--js-visible');

      const closeBtn = overlay.querySelector('#overlay1-close');
      closeBtn.addEventListener('click', hideOverlayHandler);
      btnImDeeplyInsulted.removeEventListener('click', showOverlayHandler);
      document.addEventListener('keydown', escPressHandler);
    };

    const hideOverlayHandler = function () {
      const closeBtn = overlay.querySelector('#overlay1-close');
      closeBtn.removeEventListener('click', hideOverlayHandler);
      btnImDeeplyInsulted.addEventListener('click', showOverlayHandler);
      document.removeEventListener('keydown', escPressHandler);

      window.utils.hideModal(overlay, 'overlay--js-shown', 'overlay--js-visible');
    };

    btnImDeeplyInsulted.addEventListener('click', showOverlayHandler);
  }
})();
